$(document).ready(
			function() {
				$("#searchUserCategory").change(
						function() {
							if ($("#searchUserCategory").find(":selected")
									.text() === "All") {
								$.ajax({
									url : '/users/getAllTeachersAndStudents',
									data : {

									},
									success : function(users) {
										var txt = "";
										$.each(users, function(i, user) {
											txt += "<tr>";
											txt += "<td><span>"
													+ user.firstName
													+ "</span></td>";
											txt += "<td><span>" + user.lastName
													+ "</span></td>";
											txt += "<td><span>" + user.username
													+ "</span></td>";
											txt += "<td><span>" + user.email
													+ "</span></td>";
											txt += "</tr>";
										});
										$("table tbody").empty();
										$("table tbody").append(txt);
									}
								});
							} else if($("#searchUserCategory").find(":selected").text() === "Admin") {
								var x = $("#hidden").text();
								$.ajax({
									url : '/users/getAdmin',
									data : {
										adminUsername : x
									},
									success : function(admin) {
										var txt = "";	
										
										txt += "<tr>";
											txt += "<td><span>" + admin.firstName + "</span></td>"; 
											txt += "<td><span>" + admin.lastName + "</span></td>";
											txt += "<td><span>" + admin.username + "</span></td>";
											txt += "<td><span>" + admin.email + "</span></td>";
										txt += "</tr>";								
										
										$("table tbody").empty();
										$("table tbody").append(txt);
									}
								});	
							} else if ($("#searchUserCategory").find(
									":selected").text() === "Teacher") {
								$.ajax({
									url : '/users/getAllTeachers',
									data : {

									},
									success : function(teachers) {
										var txt = "";
										$.each(teachers, function(i, teacher) {
											txt += "<tr>";
											txt += "<td><span>"
													+ teacher.firstName
													+ "</span></td>";
											txt += "<td><span>"
													+ teacher.lastName
													+ "</span></td>";
											txt += "<td><span>"
													+ teacher.username
													+ "</span></td>";
											txt += "<td><span>" + teacher.email
													+ "</span></td>";
											txt += "</tr>";
										});
										$("table tbody").empty();
										$("table tbody").append(txt);
									}
								});
							} else if ($("#searchUserCategory").find(
									":selected").text() === "Student") {
								$.ajax({
									url : '/users/getAllStudents',
									data : {

									},
									success : function(students) {
										var txt = "";
										$.each(students, function(i, student) {
											txt += "<tr>";
											txt += "<td><span>"
													+ student.firstName
													+ "</span></td>";
											txt += "<td><span>"
													+ student.lastName
													+ "</span></td>";
											txt += "<td><span>"
													+ student.username
													+ "</span></td>";
											txt += "<td><span>" + student.email
													+ "</span></td>";
											txt += "</tr>";
										});
										$("table tbody").empty();
										$("table tbody").append(txt);
									}
								});
							}
						});

				$("#searchText").on(
						"keyup",
						function() {
							var userCategory = $("#searchUserCategory").find(
									":selected").text();
							var searchCriteria = $("#searchCriteria").find(
									":selected").text();
							var searchText = $("#searchText").val();
							$.ajax({
								url : '/users/filterUsers',
								data : {
									userCategoryParam : userCategory,
									searchCriteriaParam : searchCriteria,
									searchTextParam : searchText
								},
								success : function(users) {
									var txt = "";
									$.each(users, function(i, user) {
										txt += "<tr>";
										txt += "<td><span>" + user.firstName
												+ "</span></td>";
										txt += "<td><span>" + user.lastName
												+ "</span></td>";
										txt += "<td><span>" + user.username
												+ "</span></td>";
										txt += "<td><span>" + user.email
												+ "</span></td>";
										txt += "</tr>";
									});
									$("table tbody").empty();
									$("table tbody").append(txt);
								}
							});
						});
				
				$("#clear").click(function() {
					$("#firstName").val("");
					$("#lastName").val("");
					$("#username").val("");
					$("#email").val("");
					$("#password").val("");
				});
				
				var row;
				$("table tbody").on("click", "tr", function() {
					row = $(this);
					$("#firstName").val(row.find("td span").eq(0).text());
					$("#lastName").val(row.find("td span").eq(1).text());
					$("#username").val(row.find("td span").eq(2).text());
					$("#email").val(row.find("td span").eq(3).text());
				});
			});