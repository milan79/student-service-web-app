$(document).ready(function() {
	$("#subjectTitle").change(function() {
		$.ajax({
			url : '/subjects/validateTitle',
			data : {
				subjectTitle : $("#subjectTitle").val()
			},
			success : function(responseText) {
				$("#subjectTitleErr").text(responseText);

				if (responseText != "") {
					$("#subjectTitle").focus();
				}
			}
		});
	});	

	$("#clear").click(function() {
		$("#subjectTitle").val("");
	});
});