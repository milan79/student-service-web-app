$(document).ready(function() {
		$("#searchText").on("keyup", function() {
			var searchCriteria = $("#searchCriteria").find(":selected").text();
			var searchText = $("#searchText").val();
				$.ajax({
					url : '/users/filterUsers',
					data : {
						userCategoryParam : 'Student',
						searchCriteriaParam : searchCriteria,
						searchTextParam : searchText
					},
					success : function(users) {
						var txt = "";
						$.each(users, function(i, student) {
							txt += "<tr>";
								txt += "<td><span>" + student.firstName + "</span></td>"; 
								txt += "<td><span>" + student.lastName + "</span></td>";
								txt += "<td><span>" + student.username + "</span></td>";
								txt += "<td><span>" + student.email + "</span></td>";
							txt += "</tr>";								
						});
						$("#selectStudentTable tbody").empty();
						$("#selectStudentTable tbody").append(txt);	
						$("#selectSubjectTable tbody").empty();
					}
				});
			});
		
		$("#clear").click(function() {
			$("#firstName").val("");
			$("#lastName").val("");
			$("#username").val("");
			$("#email").val("");
			$("#formSubjectTitle").val("");
		});	
		
		var row;
		var studentUsername;
		$("#selectStudentTable tbody").on("click", "tr", function() {
			row = $(this);
			$("#firstName").val(row.find("td span").eq(0).text());
			$("#lastName").val(row.find("td span").eq(1).text());
			$("#username").val(row.find("td span").eq(2).text());
			$("#email").val(row.find("td span").eq(3).text());
			
			studentUsername = $("#username").val();
			
			$.ajax({
				url: '/admin/students/notStudentSubjects',
				data: {
					studentUsernameParam : studentUsername
				},
				success: function(subjects) {
					var txt = "";
					$.each(subjects, function(i, subject) {
						txt += "<tr>";
						txt += "<td><span>" + subject.title + "</span></td>";
						txt += "</tr>";
					});
					
					$("#selectSubjectTable tbody").empty();
					$("#selectSubjectTable tbody").append(txt);
				}
			});
		});
		
		$("#searchText2").on("keyup", function() {
			var searchText = $("#searchText2").val();
				$.ajax({
					url : '/admin/students/filterNotStudentSubjects',
					data : {
						currentStudentUsernameParam : studentUsername,
						searchTextParam : searchText
					},
					success : function(subjects) {
						var txt = "";
						$.each(subjects, function(i, subject) {
							txt += "<tr>";
								txt += "<td><span>" + subject + "</span></td>";
							txt += "</tr>";								
						});
						$("#selectSubjectTable tbody").empty();
						$("#selectSubjectTable tbody").append(txt);					
					}
				});
			});
		
		$("#selectSubjectTable tbody").on("click", "tr", function() {
			row = $(this);
			$("#formSubjectTitle").val(row.find("td span").eq(0).text());
		});
	});



