$(document).ready(function() {
		$("#searchText").on("keyup", function() {
			var searchCriteria = $("#searchCriteria").find(":selected").text();
			var searchText = $("#searchText").val();
				$.ajax({
					url : '/users/filterUsers',
					data : {
						userCategoryParam : 'Student',
						searchCriteriaParam : searchCriteria,
						searchTextParam : searchText
					},
					success : function(students) {
						var txt = "";
						$.each(students, function(i, student) {
							txt += "<tr>";
								txt += "<td><span>" + student.firstName + "</span></td>"; 
								txt += "<td><span>" + student.lastName + "</span></td>";
								txt += "<td><span>" + student.username + "</span></td>";
								txt += "<td><span>" + student.email + "</span></td>";
							txt += "</tr>";								
						});
						$("#selectStudentTable tbody").empty();
						$("#selectStudentTable tbody").append(txt);	
						$("#selectTeacherTable tbody").empty();
					}
				});
			});
		
		$("#clear").click(function() {
			$("#firstName").val("");
			$("#lastName").val("");
			$("#username").val("");
			$("#email").val("");
			$("#formTeacherUsername").val("");
		});	
		
		var row;
		var studentUsername;
		$("#selectStudentTable tbody").on("click", "tr", function() {
			row = $(this);
			$("#firstName").val(row.find("td span").eq(0).text());
			$("#lastName").val(row.find("td span").eq(1).text());
			$("#username").val(row.find("td span").eq(2).text());
			$("#email").val(row.find("td span").eq(3).text());
			
			studentUsername = $("#username").val();
			
			$.ajax({
				url: '/admin/students/studentTeachers',
				data: {
					studentUsernameParam : studentUsername
				},
				success: function(teachers) {
					var txt = "";
					$.each(teachers, function(i, teacher) {
						txt += "<tr>";
							txt += "<td><span>" + teacher.firstName + "</span></td>"; 
							txt += "<td><span>" + teacher.lastName + "</span></td>";
							txt += "<td><span>" + teacher.username + "</span></td>";
							txt += "<td><span>" + teacher.email + "</span></td>";
						txt += "</tr>";						
					});
					
					$("#selectTeacherTable tbody").empty();
					$("#selectTeacherTable tbody").append(txt);
				}
			});
		});
		
		$("#searchText2").on("keyup", function() {
			var searchCriteria = $("#searchCriteria2").find(":selected").text();
			var searchText = $("#searchText2").val();
				$.ajax({
					url : '/admin/students/filterStudentTeachers',
					data : {
						searchCriteriaParam : searchCriteria,
						currentStudentUsernameParam : studentUsername,
						searchTextParam : searchText
					},
					success : function(teachers) {
						var txt = "";
						$.each(teachers, function(i, teacher) {
							txt += "<tr>";
								txt += "<td><span>" + teacher.firstName + "</span></td>"; 
								txt += "<td><span>" + teacher.lastName + "</span></td>";
								txt += "<td><span>" + teacher.username + "</span></td>";
								txt += "<td><span>" + teacher.email + "</span></td>";
							txt += "</tr>";		
						});
						$("#selectTeacherTable tbody").empty();
						$("#selectTeacherTable tbody").append(txt);					
					}
				});
			});
		
		$("#selectTeacherTable tbody").on("click", "tr", function() {
			row = $(this);
			$("#formTeacherUsername").val(row.find("td span").eq(2).text());
		});
	});



