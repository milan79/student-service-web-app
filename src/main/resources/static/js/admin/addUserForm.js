$(document).ready(function() {
	$("#username").change(function() {
		$.ajax({
			url : '/users/validateUsername',
			data : {
				username : $("#username").val()
			},
			success : function(responseText) {
				$("#usernameErr").text(responseText);

				if (responseText != "") {
					$("#username").focus();
				}
			}
		});
	});

	$("#email").change(function() {
		$.ajax({
			url : '/users/validateEmail',
			data : {
				email : $("#email").val()
			},
			success : function(responseText) {
				$("#emailErr").text(responseText);

				if (responseText != "") {
					$("#email").focus();
				}
			}
		});
	});

	$("#clear").click(function() {
		$("#firstName").val("");
		$("#lastName").val("");
		$("#username").val("");
		$("#email").val("");
		$("#password").val("");
	});
});