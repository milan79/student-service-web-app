$(document).ready(function() {
		$("#searchText").on("keyup", function() {
			var searchCriteria = $("#searchCriteria").find(":selected").text();
			var searchText = $("#searchText").val();
				$.ajax({
					url : '/users/filterUsers',
					data : {
						userCategoryParam : 'Teacher',
						searchCriteriaParam : searchCriteria,
						searchTextParam : searchText
					},
					success : function(teachers) {
						var txt = "";
						$.each(teachers, function(i, teacher) {
							txt += "<tr>";
								txt += "<td><span>" + teacher.firstName + "</span></td>"; 
								txt += "<td><span>" + teacher.lastName + "</span></td>";
								txt += "<td><span>" + teacher.username + "</span></td>";
								txt += "<td><span>" + teacher.email + "</span></td>";
							txt += "</tr>";								
						});
						$("#selectTeacherTable tbody").empty();
						$("#selectTeacherTable tbody").append(txt);	
						$("#selectSubjectTable tbody").empty();
					}
				});
			});
		
		$("#clear").click(function() {
			$("#firstName").val("");
			$("#lastName").val("");
			$("#username").val("");
			$("#email").val("");
			$("#formSubjectTitle").val("");
		});	
		
		var row;
		var teacherUsername;
		$("#selectTeacherTable tbody").on("click", "tr", function() {
			row = $(this);
			$("#firstName").val(row.find("td span").eq(0).text());
			$("#lastName").val(row.find("td span").eq(1).text());
			$("#username").val(row.find("td span").eq(2).text());
			$("#email").val(row.find("td span").eq(3).text());
			
			teacherUsername = $("#username").val();
			
			$.ajax({
				url: '/admin/teachers/teacherSubjects',
				data: {
					teacherUsernameParam : teacherUsername
				},
				success: function(subjects) {
					var txt = "";
					$.each(subjects, function(i, subject) {
						txt += "<tr>";
						txt += "<td><span>" + subject.title + "</span></td>";
						txt += "</tr>";
					});
					
					$("#selectSubjectTable tbody").empty();
					$("#selectSubjectTable tbody").append(txt);
				}
			});
		});
				
		$("#searchText2").on("keyup", function() {
			var searchText = $("#searchText2").val();
				$.ajax({
					url : '/admin/teachers/filterTeacherSubjects',
					data : {
						currentTeacherUsernameParam : teacherUsername,
						searchTextParam : searchText
					},
					success : function(subjects) {
						var txt = "";
						$.each(subjects, function(i, subject) {
							txt += "<tr>";
								txt += "<td><span>" + subject.title + "</span></td>";
							txt += "</tr>";								
						});
						$("#selectSubjectTable tbody").empty();
						$("#selectSubjectTable tbody").append(txt);					
					}
				});
			});
		
		$("#selectSubjectTable tbody").on("click", "tr", function() {
			row = $(this);
			$("#formSubjectTitle").val(row.find("td span").eq(0).text());
		});
	});



