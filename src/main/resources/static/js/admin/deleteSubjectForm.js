$(document).ready(function() {
				
		$("#searchText").on("keyup", function() {
			var searchText = $("#searchText").val();
				$.ajax({
					url : '/subjects/filterSubjects',
					data : {
						searchTextParam : searchText
					},
					success : function(subjects) {
						var txt = "";
						$.each(subjects, function(i, subject) {
							txt += "<tr>";
								txt += "<td><span>" + subject.title + "</span></td>";
							txt += "</tr>";								
						});
						$("table tbody").empty();
						$("table tbody").append(txt);					
					}
				});
			});
		
		$("#clear").click(function() {
			$("#subjectTitle").val("");
		});	
		
		var row;
		$("table tbody").on("click", "tr", function() {
			row = $(this);
			$("#subjectTitle").val(row.find("td span").eq(0).text());
		});
	});