package com.mvujanic.student.service.services;

import java.util.List;

import com.mvujanic.student.service.entities.Teacher;

public interface AdminTeacherService {
	public Teacher findTeacherByUsername(String username);
	
	public Teacher findTeacherById(int teacherId);
	
	public List<Teacher> findAllTeachers();
	
	public List<Teacher> filterStudentTeachersByUsername(int studentId, String searchText);

	public List<Teacher> filterStudentTeachersByFirstName(int studentId, String searchText);
	
	public List<Teacher> filterStudentTeachersByLastName(int studentId, String searchText);
	
	public List<Teacher> filterStudentTeachersByEmail(int studentId, String searchText);
	
	public List<Teacher> filterNotStudentTeachersByUsername(int studentId, String searchText);

	public List<Teacher> filterNotStudentTeachersByFirstName(int studentId, String searchText);
	
	public List<Teacher> filterNotStudentTeachersByLastName(int studentId, String searchText);
	
	public List<Teacher> filterNotStudentTeachersByEmail(int studentId, String searchText);
	
}
