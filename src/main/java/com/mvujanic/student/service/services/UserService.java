package com.mvujanic.student.service.services;

import java.util.List;

import com.mvujanic.student.service.entities.User;

public interface UserService {
	public User verifyLogin(String username, String email);
	
	public void saveUser(User user);
	
	public User findByUsername(String username);
	
	public User findByEmail(String email);
	
	public List<User> findAll();
	
	public void deleteUser(User user);
	
	public void deleteUserById(int id);
	
	public List<User> getAdmins();
	
	public List<User> getTeachers();
	
	public List<User> getStudents();
	
	public List<User> getTeachersAndStudents();
	
	public List<User> filterUsersByUsername(int userCategoryId, String searchText);
	
	public List<User> filterUsersByFirstName(int userCategoryId, String searchText);
	
	public List<User> filterUsersByLastName(int userCategoryId, String searchText);
	
	public List<User> filterUsersByEmail(int userCategoryId, String searchText);
	
	public List<User> filterUsersByUsername(String searchText);
	
	public List<User> filterUsersByFirstName(String searchText);
	
	public List<User> filterUsersByLastName(String searchText);
	
	public List<User> filterUsersByEmail(String searchText);
	
	public void updateUser(String firstName, String lastName, String password, String username);
}























