package com.mvujanic.student.service.services;

import java.util.List;

import com.mvujanic.student.service.entities.Subject;

public interface SubjectService {
	
	public Subject saveSubject(Subject subject);
	
	public void deleteSubject(Subject subject);
	
	public void updateSubject(String subjectTitle, int subjectId);
	
	public Subject findSubject(String title);
	
	public Subject findSubjectById(int id);
	
	public List<Subject> findAllSubjects();
	
	public List<Subject> filterSubjects(String title);
	
	public List<Subject> getTeacherSubjects(int teacherId);
	
	public List<Subject> filterTeacherSubjects(String teacherUsername, String searchText);
	
	public List<String> filterNotTeacherSubjects(String teacherUsername, String searchText);
	
	public List<Subject> filterStudentSubjects(String studentUsername, String searchText);
	
	public List<String> filterNotStudentSubjects(String studentUsername, String searchText);
	
	public void assignTeacherToSubject(int teacherId, int subjectId);
	
	public void removeTeacherFromSubject(int teacherId, int subjectId);
	
	public void assignStudentToSubject(String studentUsername, String subjectTitle);
	
	public void removeStudentFromSubject(String studentUsername, String subjectTitle);
}
