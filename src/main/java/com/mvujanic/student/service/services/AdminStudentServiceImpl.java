package com.mvujanic.student.service.services;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mvujanic.student.service.entities.Student;
import com.mvujanic.student.service.entities.Teacher;
import com.mvujanic.student.service.repositories.AdminStudentRepository;
import com.mvujanic.student.service.repositories.AdminTeacherRepository;

@Service
public class AdminStudentServiceImpl implements AdminStudentService {

	@Autowired
	private AdminStudentRepository adminStudentRepository;
	
	@Autowired
	private AdminTeacherRepository adminTeacherRepository;
	
	@Override
	public Student getStudentByUsername(String studentUsername) {
		return adminStudentRepository.findStudentByUsername(studentUsername);
	}

	@Override
	public List<Student> getAll() {
		List<Student> students = adminStudentRepository.findAll();
		Collections.sort(students);
		
		return students;
	}

	@Override
	public Student findById(String studentId) {
		return adminStudentRepository.findById(studentId);
	}

	@Override
	public void assignStudentToTeacher(String studentUsername, String teacherUsername) {
		Student student = adminStudentRepository.findStudentByUsername(studentUsername);
		
		Teacher teacher = adminTeacherRepository.findByUsername(teacherUsername);
		
		student.addTeacher(teacher);
		
		adminStudentRepository.save(student);
		
	}

	@Override
	public void removeStudentFromTeacher(String studentUsername, String teacherUsername) {
		Student student = adminStudentRepository.findStudentByUsername(studentUsername);
		
		Teacher teacher = adminTeacherRepository.findByUsername(teacherUsername);
		
		student.removeTeacher(teacher);
		
		adminStudentRepository.save(student);
		
	}

}
