package com.mvujanic.student.service.services;

import java.util.Collections;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mvujanic.student.service.entities.User;
import com.mvujanic.student.service.repositories.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
		
	@Override
	public User verifyLogin(String username, String email) {
		return userRepository.findByUsernameOrEmail(username, email);
	}

	@Override
	public void saveUser(User user) {
		userRepository.save(user);
	}

	@Override
	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	@Override
	public User findByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public List<User> findAll() {
		List<User> users = userRepository.findAll();
		Collections.sort(users);
		
		return users;
	}

	@Override
	public void deleteUser(User user) {
		userRepository.delete(user);		
	}

	@Override
	public void deleteUserById(int id) {
		userRepository.deleteById(id);		
	}

	@Override
	public List<User> getAdmins() {
		List<User> admins = userRepository.findAllAdmins();
		Collections.sort(admins);
		
		return admins;
	}

	@Override
	public List<User> getTeachers() {
		List<User> teachers = userRepository.findAllTeachers();
		Collections.sort(teachers);
		
		return teachers;
	}

	@Override
	public List<User> getStudents() {
		List<User> students = userRepository.findAllStudents();
		Collections.sort(students);
		
		return students;
	}

	@Override
	public List<User> filterUsersByUsername(int userCategoryId, String searchText) {
		List<User> users = userRepository.filterUsersByUsername(userCategoryId, searchText);
		Collections.sort(users);
		
		return users;
	}

	@Override
	public List<User> filterUsersByFirstName(int userCategoryId, String searchText) {
		List<User> users = userRepository.filterUsersByFirstName(userCategoryId, searchText);
		Collections.sort(users);
		
		return users;
	}

	@Override
	public List<User> filterUsersByLastName(int userCategoryId, String searchText) {
		List<User> users = userRepository.filterUsersByLastName(userCategoryId, searchText);
		Collections.sort(users);
		
		return users;
	}

	@Override
	public List<User> filterUsersByEmail(int userCategoryId, String searchText) {
		List<User> users = userRepository.filterUsersByEmail(userCategoryId, searchText);
		Collections.sort(users);
		
		return users;
	}

	@Override
	public List<User> filterUsersByUsername(String searchText) {
		List<User> users = userRepository.filterUsersByUsername(searchText);
		Collections.sort(users);
		
		return users;
	}

	@Override
	public List<User> filterUsersByFirstName(String searchText) {
		List<User> users = userRepository.filterUsersByFirstName(searchText);
		Collections.sort(users);
		
		return users;
	}

	@Override
	public List<User> filterUsersByLastName(String searchText) {
		List<User> users = userRepository.filterUsersByLastName(searchText);
		Collections.sort(users);
		
		return users;
	}

	@Override
	public List<User> filterUsersByEmail(String searchText) {
		List<User> users = userRepository.filterUsersByEmail(searchText);
		Collections.sort(users);
		
		return users;
	}

	@Override
	public List<User> getTeachersAndStudents() {
		List<User> users = userRepository.findAllTeachersAndStudents();
		Collections.sort(users);
		
		return users;
	}
	
	@Override
	@Transactional
	public void updateUser(String firstName, String lastName, String password, String username) {
		userRepository.updateUser(firstName, lastName, password, username);
	}

}
