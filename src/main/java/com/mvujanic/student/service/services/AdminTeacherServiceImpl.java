package com.mvujanic.student.service.services;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mvujanic.student.service.entities.Subject;
import com.mvujanic.student.service.entities.Teacher;
import com.mvujanic.student.service.repositories.AdminTeacherRepository;
import com.mvujanic.student.service.repositories.SubjectRepository;

@Service
public class AdminTeacherServiceImpl implements AdminTeacherService {

	@Autowired
	private AdminTeacherRepository adminTeacherRepository;
	
	@Autowired
	private SubjectRepository subjectRepository;
	
	@Override
	public Teacher findTeacherByUsername(String username) {
		return adminTeacherRepository.findByUsername(username);
	}

	@Override
	public Teacher findTeacherById(int teacherId) {
		return adminTeacherRepository.findById(teacherId);
	}

	@Override
	public List<Teacher> findAllTeachers() {
		List<Teacher> teachers = adminTeacherRepository.findAll();
		Collections.sort(teachers);
		
		return teachers;
	}

	@Override
	public List<Teacher> filterNotStudentTeachersByUsername(int studentId, String searchText) {
		List<Teacher> teachers = adminTeacherRepository.filterNotStudentTeachersByUsername(studentId, searchText);
		Collections.sort(teachers);
		
		return teachers;		
	}

	@Override
	public List<Teacher> filterNotStudentTeachersByFirstName(int studentId, String searchText) {
		List<Teacher> teachers = adminTeacherRepository.filterNotStudentTeachersByFirstName(studentId, searchText);
		Collections.sort(teachers);
		
		return teachers;
	}

	@Override
	public List<Teacher> filterNotStudentTeachersByLastName(int studentId, String searchText) {
		List<Teacher> teachers = adminTeacherRepository.filterNotStudentTeachersByLastName(studentId, searchText);
		Collections.sort(teachers);
		
		return teachers;
	}

	@Override
	public List<Teacher> filterNotStudentTeachersByEmail(int studentId, String searchText) {
		List<Teacher> teachers = adminTeacherRepository.filterNotStudentTeachersByEmail(studentId, searchText);
		Collections.sort(teachers);
		
		return teachers;
	}

	@Override
	public List<Teacher> filterStudentTeachersByUsername(int studentId, String searchText) {
		List<Teacher> teachers = adminTeacherRepository.filterStudentTeachersByUsername(studentId, searchText);
		Collections.sort(teachers);
		
		return teachers;
	}

	@Override
	public List<Teacher> filterStudentTeachersByFirstName(int studentId, String searchText) {
		List<Teacher> teachers = adminTeacherRepository.filterStudentTeachersByFirstName(studentId, searchText);
		Collections.sort(teachers);
		
		return teachers;
	}

	@Override
	public List<Teacher> filterStudentTeachersByLastName(int studentId, String searchText) {
		List<Teacher> teachers = adminTeacherRepository.filterStudentTeachersByLastName(studentId, searchText);
		Collections.sort(teachers);
		
		return teachers;
	}

	@Override
	public List<Teacher> filterStudentTeachersByEmail(int studentId, String searchText) {
		List<Teacher> teachers = adminTeacherRepository.filterStudentTeachersByEmail(studentId, searchText);
		Collections.sort(teachers);
		
		return teachers;
	}

	

}
