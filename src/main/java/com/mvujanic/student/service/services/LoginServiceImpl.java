package com.mvujanic.student.service.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mvujanic.student.service.entities.User;
import com.mvujanic.student.service.repositories.LoginRepository;

@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	private LoginRepository loginRepository;

	@Override
	public User verifyLogin(String username, String email) {
		User user = loginRepository.findByUsernameOrEmail(username, email);

		return user;

	}

}
