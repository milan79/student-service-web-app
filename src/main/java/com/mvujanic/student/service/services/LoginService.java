package com.mvujanic.student.service.services;

import com.mvujanic.student.service.entities.User;

public interface LoginService {
	public User verifyLogin(String username, String email);
}
