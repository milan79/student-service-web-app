package com.mvujanic.student.service.services;

import java.util.List;

import com.mvujanic.student.service.entities.Student;

public interface AdminStudentService {
	
	public Student getStudentByUsername(String studentUsername);
	
	public List<Student> getAll();
	
	public Student findById(String studentId);
	
	public void assignStudentToTeacher(String studentUsername, String teacherUsername);
	
	public void removeStudentFromTeacher(String studentUsername, String teacherUsername);
}
