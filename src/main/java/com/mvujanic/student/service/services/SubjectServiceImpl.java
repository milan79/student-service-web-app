package com.mvujanic.student.service.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mvujanic.student.service.entities.Student;
import com.mvujanic.student.service.entities.Subject;
import com.mvujanic.student.service.entities.Teacher;
import com.mvujanic.student.service.repositories.AdminStudentRepository;
import com.mvujanic.student.service.repositories.AdminTeacherRepository;
import com.mvujanic.student.service.repositories.SubjectRepository;

@Service
public class SubjectServiceImpl implements SubjectService {

	@Autowired
	private SubjectRepository subjectRepository;
	
	@Autowired
	private AdminTeacherRepository adminTeacherRepository;
	
	@Autowired
	private AdminStudentRepository adminStudentRepository;
	
	@Override
	public Subject saveSubject(Subject subject) {
		return subjectRepository.save(subject); 
	}

	@Override
	public void deleteSubject(Subject subject) {
		subjectRepository.delete(subject);
	}

	@Override
	@Transactional
	public void updateSubject(String subjectTitle, int subjectId) {
		subjectRepository.updateSubject(subjectTitle, subjectId);
	}

	@Override
	public Subject findSubject(String title) {
		return subjectRepository.findByTitle(title);
	}

	@Override
	public List<Subject> findAllSubjects() {
		List<Subject> subjects = subjectRepository.findAll();
		Collections.sort(subjects);
		
		return subjects;
	}

	@Override
	public List<Subject> filterSubjects(String title) {
		List<Subject> subjects = subjectRepository.filterSubjectsByTitle(title);
		Collections.sort(subjects);
		
		return subjects;
	}

	@Override
	public Subject findSubjectById(int id) {
		return subjectRepository.findById(id);
	}

	@Override
	public List<Subject> getTeacherSubjects(int teacherId) {
		List<Subject> subjects = subjectRepository.getTeacherSubjects(teacherId);
		Collections.sort(subjects);
		
		return subjects;
	}

	@Override
	public void assignTeacherToSubject(int teacherId, int subjectId) {
		Teacher teacher = adminTeacherRepository.findById(teacherId);
		
		Subject subject = subjectRepository.findById(subjectId);
		
		subject.addTeacher(teacher);
		
		subjectRepository.save(subject);
	}

	@Override
	public void removeTeacherFromSubject(int teacherId, int subjectId) {
		Teacher teacher = adminTeacherRepository.findById(teacherId);
		
		Subject subject = subjectRepository.findById(subjectId);
		
		subject.removeTeacher(teacher);
		
		subjectRepository.save(subject);
	}

	@Override
	public void assignStudentToSubject(String studentUsername, String subjectTitle) {
		Student student = adminStudentRepository.findStudentByUsername(studentUsername);
		
		Subject subject = subjectRepository.findByTitle(subjectTitle);
		
		subject.addStudent(student);
		
		subjectRepository.save(subject);
	}

	@Override
	public void removeStudentFromSubject(String studentUsername, String subjectTitle) {
		Student student = adminStudentRepository.findStudentByUsername(studentUsername);
		
		Subject subject = subjectRepository.findByTitle(subjectTitle);
		
		subject.removeStudent(student);
		
		subjectRepository.save(subject);
		
	}

	@Override
	public List<Subject> filterTeacherSubjects(String teacherUsername, String searchText) {
		Teacher teacher = adminTeacherRepository.findByUsername(teacherUsername);
		
		List<Subject> subjects = subjectRepository.filterTeacherSubjects(teacher.getId(), searchText);
		Collections.sort(subjects);
		
		return subjects;
	}

	@Override
	public List<String> filterNotTeacherSubjects(String teacherUsername, String searchText) {
		Teacher teacher = adminTeacherRepository.findByUsername(teacherUsername);
		
		List<String> subjects = subjectRepository.filterNotTeacherSubjects(teacher.getId(), searchText);
		Collections.sort(subjects);
		
		return subjects;		
	}

	@Override
	public List<Subject> filterStudentSubjects(String studentUsername, String searchText) {
		Student student = adminStudentRepository.findStudentByUsername(studentUsername);
		
		List<Subject> subjects = subjectRepository.filterStudentSubjects(student.getId(), searchText);
		Collections.sort(subjects);
		
		return subjects;
	}

	@Override
	public List<String> filterNotStudentSubjects(String studentUsername, String searchText) {
		Student student = adminStudentRepository.findStudentByUsername(studentUsername);
		
		List<String> subjects = subjectRepository.filterNotStudentSubjects(student.getId(), searchText);
		Collections.sort(subjects);
		
		return subjects;
	}

}
















