package com.mvujanic.student.service.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mvujanic.student.service.entities.User;
import com.mvujanic.student.service.services.LoginService;

@Controller
@RequestMapping("/student-service")
public class LoginController {

	@Autowired
	private LoginService loginService;
	
	@RequestMapping("/login")
	public String getLoginForm(Model model) {
		User user = new User();
		
		model.addAttribute("user", user);
		
		return "login";
	}
	
	@PostMapping("/loginVerification")
	public String verifyLogin(@ModelAttribute("user") User user, Model model) {
		User userFromDb = loginService.verifyLogin(user.getUsername(), user.getUsername());
		
		if(userFromDb != null && (userFromDb.getPassword().contentEquals(user.getPassword()))) {
			model.addAttribute("user", userFromDb);
			if (userFromDb.getUserCategory().getDescription().contentEquals("admin")) {
				return "/admin/adminIndex";
			} else if (userFromDb.getUserCategory().getDescription().contentEquals("teacher")) {
				return "/teacher/teacherIndex";
			} else {
				return "/student/studentIndex";
			}
		} 
		
		String errMsg = "Invalid username/password. Try again.";
		model.addAttribute("errMsg", errMsg);
		
		String wrongUser = user.getUsername();
		model.addAttribute("wrongUser", wrongUser);
				
		return "login";
	}
}






