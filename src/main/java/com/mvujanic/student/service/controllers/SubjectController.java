package com.mvujanic.student.service.controllers;

import java.util.List;

import javax.swing.JOptionPane;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mvujanic.student.service.entities.Subject;
import com.mvujanic.student.service.entities.User;
import com.mvujanic.student.service.services.SubjectService;
import com.mvujanic.student.service.services.UserService;

@Controller
@RequestMapping("/subjects")
public class SubjectController {
	
	private User currentUser;

	@Autowired
	private SubjectService subjectService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("/addSubjectForm")
	public String addSubjectForm(@RequestParam("currentUsername") String currentUsername, Model model) {
		User user = userService.findByUsername(currentUsername);
		
		currentUser = user;
		
		model.addAttribute("user", currentUser);
		
		Subject new_subject = new Subject();
		model.addAttribute("new_subject", new_subject);
		
		return "/admin/addSubjectForm";
	}
	
	@RequestMapping("/deleteSubjectForm")
	public String deleteSubjectForm(@RequestParam("currentUsername") String currentUsername, Model model) {
		User user = userService.findByUsername(currentUsername);
		
		currentUser = user;
		
		model.addAttribute("user", currentUser);
		
		Subject new_subject = new Subject();
		model.addAttribute("new_subject", new_subject);
		
		List<Subject> subjects = subjectService.findAllSubjects();
		model.addAttribute("subjects", subjects);
		
		return "/admin/deleteSubjectForm";
	}
	
	@RequestMapping("/updateSubjectForm")
	public String updateSubjectForm(@RequestParam("currentUsername") String currentUsername, Model model) {
		User user = userService.findByUsername(currentUsername);
		
		currentUser = user;
		
		model.addAttribute("user", currentUser);
		
		Subject new_subject = new Subject();
		model.addAttribute("new_subject", new_subject);
		
		List<Subject> subjects = subjectService.findAllSubjects();
		model.addAttribute("subjects", subjects);
		
		int id = 0;
		model.addAttribute("id", id);
		
		return "/admin/updateSubjectForm";
	}
	
	@RequestMapping("/validateTitle")
	public @ResponseBody String validateTitle(@RequestParam("subjectTitle") String subjectTitle) {
		Subject subject = subjectService.findSubject(subjectTitle);
		
		String msg = "";
		
		if (subject != null) {
			msg = "Subject '" + subjectTitle.toUpperCase() + "' already exists";
		}
		
		return msg;
	}
	
	@RequestMapping("/addSubject") 
	public String addSubject(@ModelAttribute("new_subject") Subject subject, Model model) {
		model.addAttribute("user", currentUser);
		
		subjectService.saveSubject(subject);
		
		JOptionPane.showMessageDialog(null, "Subject " + subject.getTitle() + "\nsuccessfully saved to database");
		
		return "/admin/addSubjectForm";
	}
	
	@RequestMapping("/deleteSubject")
	public String deleteSubject(@ModelAttribute("new_subject") Subject subject, Model model) {
		model.addAttribute("user", currentUser);
		
		Subject subjectDb = subjectService.findSubject(subject.getTitle());
		
		subjectService.deleteSubject(subjectDb);
		
		List<Subject> subjects = subjectService.findAllSubjects();
		model.addAttribute("subjects", subjects);
		
		JOptionPane.showMessageDialog(null, "Subject successfully deleted");
		
		return "/admin/deleteSubjectForm";
	}
	
	@RequestMapping("/updateSubject")
	public String updateSubject(@ModelAttribute("new_subject") Subject subject, Model model) {
		model.addAttribute("user", currentUser);
		
		subjectService.updateSubject(subject.getTitle(), subject.getId());
		
		List<Subject> subjects = subjectService.findAllSubjects();
		model.addAttribute("subjects", subjects);
		
		JOptionPane.showMessageDialog(null, "Subject successfully updated");
		
		return "/admin/updateSubjectForm";
	}
	
	@RequestMapping("/filterSubjects")
	public @ResponseBody List<Subject> filterSubjects(@RequestParam("searchTextParam") String searchText) {
		List<Subject> subjects = subjectService.filterSubjects(searchText);
		
		return subjects;
	}
	
	@RequestMapping("/goToIndex")
	public String goToIndex(Model model) {
		model.addAttribute("user", currentUser);
		
		return "/admin/adminIndex";
	}
	
}
