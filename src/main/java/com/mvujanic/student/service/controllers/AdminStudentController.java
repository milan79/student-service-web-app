package com.mvujanic.student.service.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JOptionPane;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mvujanic.student.service.entities.Student;
import com.mvujanic.student.service.entities.Subject;
import com.mvujanic.student.service.entities.Teacher;
import com.mvujanic.student.service.entities.User;
import com.mvujanic.student.service.services.AdminStudentService;
import com.mvujanic.student.service.services.AdminTeacherService;
import com.mvujanic.student.service.services.SubjectService;
import com.mvujanic.student.service.services.UserService;

@Controller
@RequestMapping("/admin/students")
public class AdminStudentController {

	@Autowired
	private UserService userService;

	@Autowired
	private SubjectService subjectService;

	@Autowired
	private AdminStudentService adminStudentService;

	@Autowired
	private AdminTeacherService adminTeacherService;

	private User currentUser;

	@RequestMapping("/assignStudentToSubjectForm")
	public String assignStudentToSubject(@RequestParam("currentUsername") String username, Model model) {
		User user = userService.findByUsername(username);

		currentUser = user;
		model.addAttribute("user", currentUser);

		List<Student> students = adminStudentService.getAll();
		model.addAttribute("students", students);

		User newUser = new User();
		model.addAttribute("new_user", newUser);

		return "/admin/assignStudentToSubjectForm";
	}

	@RequestMapping("/removeStudentFromTeacherForm")
	public String removeStudentFromTeacher(@RequestParam("currentUsername") String username, Model model) {
		User user = userService.findByUsername(username);

		currentUser = user;
		model.addAttribute("user", currentUser);

		List<Student> students = adminStudentService.getAll();
		model.addAttribute("students", students);

		User newUser = new User();
		model.addAttribute("new_user", newUser);

		return "/admin/removeStudentFromTeacherForm";
	}

	@RequestMapping("/removeStudentFromSubjectForm")
	public String removeStudentFromSubject(@RequestParam("currentUsername") String username, Model model) {
		User user = userService.findByUsername(username);

		currentUser = user;
		model.addAttribute("user", currentUser);

		List<Student> students = adminStudentService.getAll();
		model.addAttribute("students", students);

		User newUser = new User();
		model.addAttribute("new_user", newUser);

		return "/admin/removeStudentFromSubjectForm";
	}

	@RequestMapping("/notStudentSubjects")
	public @ResponseBody List<Subject> getNotStudentSubjects(
			@RequestParam("studentUsernameParam") String studentUsername) {

		Student student = adminStudentService.getStudentByUsername(studentUsername);

		List<Subject> studentSubjects = student.getSubjects();

		List<Subject> allSubjects = subjectService.findAllSubjects();

		Set<Subject> ss = new HashSet<Subject>(studentSubjects);

		Set<Subject> as = new HashSet<Subject>(allSubjects);

		as.removeAll(ss);

		List<Subject> notStudentSubjects = new ArrayList<>(as);
		
		Collections.sort(notStudentSubjects);

		return notStudentSubjects;
	}

	@RequestMapping("/notStudentTeachers")
	public @ResponseBody List<Teacher> getNotStudentTeachers(
			@RequestParam("studentUsernameParam") String studentUsername) {

		Student student = adminStudentService.getStudentByUsername(studentUsername);

		List<Teacher> studentTeachers = student.getTeachers();

		List<Teacher> allTeachers = adminTeacherService.findAllTeachers();

		Set<Teacher> st = new HashSet<Teacher>(studentTeachers);

		Set<Teacher> at = new HashSet<Teacher>(allTeachers);

		at.removeAll(st);

		List<Teacher> notStudentTeachers = new ArrayList<>(at);
		
		Collections.sort(notStudentTeachers);

		return notStudentTeachers;
	}

	@RequestMapping("/studentSubjects")
	public @ResponseBody List<Subject> getStudentSubjects(
			@RequestParam("studentUsernameParam") String studentUsername) {

		Student student = adminStudentService.getStudentByUsername(studentUsername);

		List<Subject> subjects = student.getSubjects();

		return subjects;
	}

	@RequestMapping("/studentTeachers")
	public @ResponseBody List<Teacher> getStudentTeachers(
			@RequestParam("studentUsernameParam") String studentUsername) {

		Student student = adminStudentService.getStudentByUsername(studentUsername);

		List<Teacher> teachers = student.getTeachers();

		return teachers;
	}

	@RequestMapping("/assignToSubject")
	public String assignToSubject(@ModelAttribute("new_user") User user, Model model) {
		// int studentId =
		// adminStudentService.getStudentByUsername(user.getUsername()).getId();

		// int subjectId = subjectService.findSubject(user.getPassword()).getId();

		subjectService.assignStudentToSubject(user.getUsername(), user.getPassword());

		model.addAttribute("user", currentUser);

		List<Student> students = adminStudentService.getAll();
		model.addAttribute("students", students);

		JOptionPane.showMessageDialog(null, "Student successfully assigned to subject");

		return "/admin/assignStudentToSubjectForm";
	}

	@RequestMapping("/removeFromSubject")
	public String removeFromSubject(@ModelAttribute("new_user") User user, Model model) {

		subjectService.removeStudentFromSubject(user.getUsername(), user.getPassword());

		model.addAttribute("user", currentUser);

		List<Student> students = adminStudentService.getAll();
		model.addAttribute("students", students);

		JOptionPane.showMessageDialog(null, "Student successfully removed from subject");

		return "/admin/removeStudentFromSubjectForm";
	}

	@RequestMapping("/filterStudentSubjects")
	public @ResponseBody List<Subject> filterStudentSubjects(
			@RequestParam("currentStudentUsernameParam") String studentUsername,
			@RequestParam("searchTextParam") String searchText) {

		List<Subject> subjects = subjectService.filterStudentSubjects(studentUsername, searchText);

		return subjects;
	}

	@RequestMapping("/filterStudentTeachers")
	public @ResponseBody List<Teacher> filterStudentTeachers(
			@RequestParam("searchCriteriaParam") String searchCriteria,
			@RequestParam("currentStudentUsernameParam") String studentUsername,
			@RequestParam("searchTextParam") String searchText) {
		
		Student student = adminStudentService.getStudentByUsername(studentUsername);
		
		List<Teacher> teachers = null;
		
		if (searchCriteria.contentEquals("Username")) {
			teachers = adminTeacherService.filterStudentTeachersByUsername(student.getId(), searchText);
		} else if (searchCriteria.contentEquals("First Name")) {
			teachers = adminTeacherService.filterStudentTeachersByFirstName(student.getId(), searchText);
		} else if (searchCriteria.contentEquals("Last Name")) {
			teachers = adminTeacherService.filterStudentTeachersByLastName(student.getId(), searchText);
		} else if (searchCriteria.contentEquals("Email")) {
			teachers = adminTeacherService.filterStudentTeachersByEmail(student.getId(), searchText);
		}
		
		return teachers;
	}

	@RequestMapping("/filterNotStudentSubjects")
	public @ResponseBody List<String> filterNotTeacherSubjects(
			@RequestParam("currentStudentUsernameParam") String studentUsername,
			@RequestParam("searchTextParam") String searchText) {

		List<String> subjects = subjectService.filterNotStudentSubjects(studentUsername, searchText);

		return subjects;
	}

	@RequestMapping("/filterNotStudentTeachers")
	public @ResponseBody List<Teacher> filterNotStudentTeachers(
			@RequestParam("searchCriteriaParam") String searchCriteria,
			@RequestParam("currentStudentUsernameParam") String studentUsername,
			@RequestParam("searchTextParam") String searchText) {

		Student student = adminStudentService.getStudentByUsername(studentUsername);

		List<Teacher> teachers = null;

		if (searchCriteria.contentEquals("Username")) {
			teachers = adminTeacherService.filterNotStudentTeachersByUsername(student.getId(), searchText);
		} else if (searchCriteria.contentEquals("First Name")) {
			teachers = adminTeacherService.filterNotStudentTeachersByFirstName(student.getId(), searchText);
		} else if (searchCriteria.contentEquals("Last Name")) {
			teachers = adminTeacherService.filterNotStudentTeachersByLastName(student.getId(), searchText);
		} else if (searchCriteria.contentEquals("Email")) {
			teachers = adminTeacherService.filterNotStudentTeachersByEmail(student.getId(), searchText);
		}

		return teachers;
	}

	@RequestMapping("/assignToTeacher")
	public String assignToTeacher(@ModelAttribute("new_user") User user, Model model) {

		adminStudentService.assignStudentToTeacher(user.getUsername(), user.getPassword());

		model.addAttribute("user", currentUser);

		List<Student> students = adminStudentService.getAll();
		model.addAttribute("students", students);

		JOptionPane.showMessageDialog(null, "Student successfully assigned to teacher");

		return "/admin/assignStudentToTeacherForm";
	}
	
	@RequestMapping("/removeFromTeacher")
	public String removeFromTeacher(@ModelAttribute("new_user") User user, Model model) {
		
		adminStudentService.removeStudentFromTeacher(user.getUsername(), user.getPassword());
	
		model.addAttribute("user", currentUser);
		
		List<Student> students = adminStudentService.getAll();
		model.addAttribute("students", students);
		
		JOptionPane.showMessageDialog(null, "Student successfully removed from teacher");
		
		return "/admin/removeStudentFromTeacherForm";
	}

	@RequestMapping("/assignStudentToTeacherForm")
	public String assignStudentToTeacherForm(@RequestParam("currentUsername") String username, Model model) {
		User user = userService.findByUsername(username);
		currentUser = user;

		model.addAttribute("user", currentUser);

		List<Student> students = adminStudentService.getAll();
		model.addAttribute("students", students);

		User newUser = new User();
		model.addAttribute("new_user", newUser);

		return "/admin/assignStudentToTeacherForm";
	}

	@RequestMapping("/goToIndex")
	public String goToIndex(Model model) {
		model.addAttribute("user", currentUser);

		return "/admin/adminIndex";
	}

}
