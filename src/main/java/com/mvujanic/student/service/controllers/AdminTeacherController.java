package com.mvujanic.student.service.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JOptionPane;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mvujanic.student.service.entities.Subject;
import com.mvujanic.student.service.entities.Teacher;
import com.mvujanic.student.service.entities.User;
import com.mvujanic.student.service.services.AdminTeacherService;
import com.mvujanic.student.service.services.SubjectService;
import com.mvujanic.student.service.services.UserService;

@Controller
@RequestMapping("/admin/teachers")
public class AdminTeacherController {

	@Autowired
	private UserService userService;

	@Autowired
	private SubjectService subjectService;

	@Autowired
	private AdminTeacherService adminTeacherService;

	private User currentUser;

	@RequestMapping("/removeTeacherFromSubjectForm")
	public String removeTeacherFromSubject(@RequestParam("currentUsername") String currentUsername, Model model) {
		User user = userService.findByUsername(currentUsername);
		currentUser = user;

		model.addAttribute("user", user);

		List<User> teachers = userService.getTeachers();
		model.addAttribute("teachers", teachers);

		User newUser = new User();
		model.addAttribute("new_user", newUser);

		return "/admin/removeTeacherFromSubjectForm";
	}

	@RequestMapping("/assignTeacherToSubjectForm")
	public String assignTeacherToSubject(@RequestParam("currentUsername") String currentUsername, Model model) {
		User user = userService.findByUsername(currentUsername);
		currentUser = user;

		model.addAttribute("user", user);

		List<User> teachers = userService.getTeachers();
		model.addAttribute("teachers", teachers);

		User newUser = new User();
		model.addAttribute("new_user", newUser);

		return "/admin/assignTeacherToSubjectForm";
	}

	@RequestMapping("/assignToSubject")
	public String assignToSubject(@ModelAttribute("new_user") User user, Model model) {
		int teacherId = adminTeacherService.findTeacherByUsername(user.getUsername()).getId();

		int subjectId = subjectService.findSubject(user.getPassword()).getId();

		subjectService.assignTeacherToSubject(teacherId, subjectId);

		model.addAttribute("user", currentUser);

		List<User> teachers = userService.getTeachers();

		model.addAttribute("teachers", teachers);

		JOptionPane.showMessageDialog(null, "Teacher successfully assigned to subject");

		return "/admin/assignTeacherToSubjectForm";
	}

	@RequestMapping("/removeFromSubject")
	public String removeFromSubject(@ModelAttribute("new_user") User user, Model model) {
		int teacherId = adminTeacherService.findTeacherByUsername(user.getUsername()).getId();

		int subjectId = subjectService.findSubject(user.getPassword()).getId();

		subjectService.removeTeacherFromSubject(teacherId, subjectId);

		model.addAttribute("user", currentUser);

		List<User> teachers = userService.getTeachers();

		model.addAttribute("teachers", teachers);

		JOptionPane.showMessageDialog(null, "Teacher successfully removed from subject");

		return "/admin/assignTeacherToSubjectForm";
	}

	@RequestMapping("/teacherSubjects")
	public @ResponseBody List<Subject> getTeacherSubjects(
			@RequestParam("teacherUsernameParam") String teacherUsername) {

		Teacher teacher = adminTeacherService.findTeacherByUsername(teacherUsername);

		List<Subject> subjects = teacher.getSubjects();
		
		return subjects;
	}

	@RequestMapping("/notTeacherSubjects")
	public @ResponseBody List<Subject> getNotTeacherSubjects(
			@RequestParam("teacherUsernameParam") String teacherUsername) {

		Teacher teacher = adminTeacherService.findTeacherByUsername(teacherUsername);

		List<Subject> teacherSubjects = teacher.getSubjects();

		List<Subject> allSubjects = subjectService.findAllSubjects();

		Set<Subject> ts = new HashSet<Subject>(teacherSubjects);

		Set<Subject> as = new HashSet<Subject>(allSubjects);

		as.removeAll(ts);

		List<Subject> notTeacherSubjects = new ArrayList<>(as);
		
		Collections.sort(notTeacherSubjects);
		
		return notTeacherSubjects;
	}

	@RequestMapping("/filterTeacherSubjects")
	public @ResponseBody List<Subject> filterTeacherSubjects(
			@RequestParam("currentTeacherUsernameParam") String teacherUsername,
			@RequestParam("searchTextParam") String searchText) {

		List<Subject> subjects = subjectService.filterTeacherSubjects(teacherUsername, searchText);
		
		return subjects;
	}
	
	@RequestMapping("/filterNotTeacherSubjects")
	public @ResponseBody List<String> filterNotTeacherSubjects(
			@RequestParam("currentTeacherUsernameParam") String teacherUsername,
			@RequestParam("searchTextParam") String searchText) {

		List<String> subjects = subjectService.filterNotTeacherSubjects(teacherUsername, searchText);
				
		return subjects;
	}

	@RequestMapping("/goToIndex")
	public String goToIndex(Model model) {
		model.addAttribute("user", currentUser);

		return "/admin/adminIndex";
	}
}
