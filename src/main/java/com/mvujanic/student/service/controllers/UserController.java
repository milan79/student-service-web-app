package com.mvujanic.student.service.controllers;

import java.util.List;

import javax.swing.JOptionPane;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mvujanic.student.service.entities.Student;
import com.mvujanic.student.service.entities.Subject;
import com.mvujanic.student.service.entities.Teacher;
import com.mvujanic.student.service.entities.User;
import com.mvujanic.student.service.services.AdminStudentService;
import com.mvujanic.student.service.services.AdminTeacherService;
import com.mvujanic.student.service.services.UserService;

@Controller
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private AdminTeacherService adminTeacherService;
	
	@Autowired
	private AdminStudentService adminStudentService;
	
	private User currentUser;
	private List<User> users;

	@RequestMapping("/addUserForm")
	public String addUserForm(@RequestParam("currentUsername") String currentUsername, Model model) {
		User user = userService.verifyLogin(currentUsername, currentUsername);
				
		currentUser = user;
		model.addAttribute("user", user);
		
		User newUser = new User();
		model.addAttribute("new_user", newUser);

		return "/admin/addUserForm";
	}
	
	@RequestMapping("/updateUserForm")
	public String updateUserForm(@RequestParam("currentUsername") String currentUsername, Model model) {
		User user = userService.verifyLogin(currentUsername, currentUsername);
				
		currentUser = user;
		model.addAttribute("user", user);
		
		User newUser = new User();
		model.addAttribute("new_user", newUser);
		
		users = userService.getTeachersAndStudents();
		model.addAttribute("users", users);

		return "/admin/updateUserForm";
	}
	
	@RequestMapping("/deleteUserForm")
	public String deleteUserForm(@RequestParam("currentUsername") String currentUsername, Model model) {
		User user = userService.verifyLogin(currentUsername, currentUsername);
		
		currentUser = user;		
		model.addAttribute("user", currentUser);
		
		User newUser = new User();
		model.addAttribute("new_user", newUser);
				
		users = userService.getTeachersAndStudents();
		model.addAttribute("users", users);

		return "/admin/deleteUserForm";
	}

	@PostMapping("/addUser")
	public String addUser(@ModelAttribute("new_user") User user,
			Model model) {
		
		model.addAttribute("user", currentUser);
		
		userService.saveUser(user);

		JOptionPane.showMessageDialog(null, "User " + user.getFirstName() + " " + user.getLastName() + "\nsuccessfully saved to database");

		return "/admin/addUserForm";
	}
	
	@PostMapping("/updateUser")
	public String updateUser(@ModelAttribute("new_user") User user,
			Model model) {
		
		userService.updateUser(user.getFirstName(), user.getLastName(), user.getPassword(), user.getUsername());
		
		User userDb = userService.findByUsername(user.getUsername());
		
		if (userDb.getUserCategory().getDescription().equalsIgnoreCase("admin")) {
			model.addAttribute("user", userDb);
		} else {
			model.addAttribute("user", currentUser);
		}
		
		users = userService.getTeachersAndStudents();
		model.addAttribute("users", users);

		JOptionPane.showMessageDialog(null, "User " + user.getFirstName() + " " + user.getLastName() + " data\nsuccessfully updated");

		return "/admin/updateUserForm";
	}
	
	@PostMapping("/deleteUser")
	public String deleteUser(@ModelAttribute("new_user") User user, Model model) {
		User userDb = userService.verifyLogin(user.getUsername(), user.getUsername());
		model.addAttribute("user", currentUser);		
				
		if (userDb.getUserCategory().getDescription().equalsIgnoreCase("admin")) {
			userService.deleteUser(userDb);
			return "login";
		}
			
		userService.deleteUser(userDb);
		
		users = userService.getTeachersAndStudents();
		model.addAttribute("users", users);
		
		JOptionPane.showMessageDialog(null, "User successfully deleted");
	
		return "/admin/deleteUserForm";
	}
	
	@RequestMapping("/getAllUsers")
	public @ResponseBody List<User> getUsers() {
		return userService.findAll();
	}
	
	@RequestMapping("/getAdmin")
	public @ResponseBody User getAdmin(@RequestParam("adminUsername") String adminUsername) {
		return userService.findByUsername(adminUsername);
	}
	
	@RequestMapping("/getAllTeachersAndStudents")
	public @ResponseBody List<User> getTeachersAndStudents() {
		return userService.getTeachersAndStudents();
	}
	
	@RequestMapping("/getAllTeachers")
	public @ResponseBody List<User> getTeachers() {
		return userService.getTeachers();
	}
	
	@RequestMapping("/getAllStudents")
	public @ResponseBody List<User> getStudents() {
		return userService.getStudents();
	}
	
	@RequestMapping("/filterUsers")
	public @ResponseBody List<User> getFilteredUsers(@RequestParam("userCategoryParam") String userCategory, 
			@RequestParam("searchCriteriaParam") String searchCriteria, 
			@RequestParam("searchTextParam") String searchText) {
		
		int userCategoryId = 0;
		
		if (userCategory.contentEquals("Teacher")) {
			userCategoryId = 2;			
		} else if (userCategory.contentEquals("Student")) {
			userCategoryId = 3;			
		}
				
		List<User> users = null;
		if (userCategory.contentEquals("All")) {
			if (searchCriteria.contentEquals("Username")) {
				users = userService.filterUsersByUsername(searchText);
			} else if (searchCriteria.contentEquals("First Name")) {
				users = userService.filterUsersByFirstName(searchText);
			} else if (searchCriteria.contentEquals("Last Name")) {
				users = userService.filterUsersByLastName(searchText);
			} else if (searchCriteria.contentEquals("Email")) {
				users = userService.filterUsersByEmail(searchText);
			}
		} else {
			if (searchCriteria.contentEquals("Username")) {
				users = userService.filterUsersByUsername(userCategoryId, searchText);
			} else if (searchCriteria.contentEquals("First Name")) {
				users = userService.filterUsersByFirstName(userCategoryId, searchText);
			} else if (searchCriteria.contentEquals("Last Name")) {
				users = userService.filterUsersByLastName(userCategoryId, searchText);
			} else if (searchCriteria.contentEquals("Email")) {
				users = userService.filterUsersByEmail(userCategoryId, searchText);
			}
		}
		
		return users;
	}

	@RequestMapping("/validateUsername")
	public @ResponseBody String validateUsername(@RequestParam("username") String username) {
		User user = userService.findByUsername(username);
		String msg = "";

		if (user != null) {
			msg = "Username '" + username.toUpperCase() + "' already exists";
		}

		return msg;
	}

	@RequestMapping("/validateEmail")
	public @ResponseBody String validateEmail(@RequestParam("email") String email) {
		User user = userService.findByEmail(email);

		String msg = "";

		if (user != null) {
			msg = "Email '" + email.toUpperCase() + "' already exists";
		}

		return msg;
	}
	
	@RequestMapping("/goToIndex")
	public String goToIndex(Model model) {
		model.addAttribute("user", currentUser);
		
		return "/admin/adminIndex";
	}
	
}
