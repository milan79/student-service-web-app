package com.mvujanic.student.service.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mvujanic.student.service.entities.Teacher;

@Repository
public interface AdminTeacherRepository extends JpaRepository<Teacher, Integer> {
	public Teacher findByUsername(String username);
	
	public Teacher findById(int teacherId);
	
	@Query( value = "select * from teacher " +
					"join student_teacher " + 
					"on teacher.id = student_teacher.teacher_id " +
					"where student_teacher.student_id = ?1 " +
					"and teacher.username like ?2%", nativeQuery = true)
	List<Teacher> filterStudentTeachersByUsername(int studentId, String searchText);
	
	@Query( value = "select * from teacher " +
			"join student_teacher " + 
			"on teacher.id = student_teacher.teacher_id " +
			"where student_teacher.student_id = ?1 " +
			"and teacher.first_name like ?2%", nativeQuery = true)
	List<Teacher> filterStudentTeachersByFirstName(int studentId, String searchText);

	
	@Query( value = "select * from teacher " +
			"join student_teacher " + 
			"on teacher.id = student_teacher.teacher_id " +
			"where student_teacher.student_id = ?1 " +
			"and teacher.last_name like ?2%", nativeQuery = true)
	List<Teacher> filterStudentTeachersByLastName(int studentId, String searchText);

	
	@Query( value = "select * from teacher " +
			"join student_teacher " + 
			"on teacher.id = student_teacher.teacher_id " +
			"where student_teacher.student_id = ?1 " +
			"and teacher.email like ?2%", nativeQuery = true)
	List<Teacher> filterStudentTeachersByEmail(int studentId, String searchText);

	
	@Query( value = "select * from teacher " + 
			"where teacher.username not in " + 
			"(select teacher.username from teacher join student_teacher " + 
			"on teacher.id = student_teacher.teacher_id " + 
			"where student_teacher.student_id = ?1) and " + 
			"teacher.username like ?2%", nativeQuery = true)
	List<Teacher> filterNotStudentTeachersByUsername(int studentId, String searchText);
	
	@Query( value = "select * from teacher " + 
			"where teacher.username not in " + 
			"(select teacher.username from teacher join student_teacher " + 
			"on teacher.id = student_teacher.teacher_id " + 
			"where student_teacher.student_id = ?1) and " + 
			"teacher.first_name like ?2%", nativeQuery = true)
	List<Teacher> filterNotStudentTeachersByFirstName(int studentId, String searchText);
	
	@Query( value = "select * from teacher " + 
			"where teacher.username not in " + 
			"(select teacher.username from teacher join student_teacher " + 
			"on teacher.id = student_teacher.teacher_id " + 
			"where student_teacher.student_id = ?1) and " + 
			"teacher.last_name like ?2%", nativeQuery = true)
	List<Teacher> filterNotStudentTeachersByLastName(int studentId, String searchText);
	
	@Query( value = "select * from teacher " + 
			"where teacher.username not in " + 
			"(select teacher.username from teacher join student_teacher " + 
			"on teacher.id = student_teacher.teacher_id " + 
			"where student_teacher.student_id = ?1) and " + 
			"teacher.email like ?2%", nativeQuery = true)
	List<Teacher> filterNotStudentTeachersByEmail(int studentId, String searchText);
}
