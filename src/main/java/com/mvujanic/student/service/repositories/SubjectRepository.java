package com.mvujanic.student.service.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mvujanic.student.service.entities.Subject;
import com.mvujanic.student.service.entities.Teacher;
import com.mvujanic.student.service.entities.User;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, Integer> {

	public Subject findByTitle(String title);
	
	public Subject findById(int subjectId);
	
	@Modifying
	@Query("UPDATE Subject SET title = ?1 WHERE id = ?2")
	public void updateSubject(String subjectTitle, int subjectId);
	
	@Query("SELECT s FROM Subject s WHERE s.title LIKE ?1%")
	List<Subject> filterSubjectsByTitle(String title);
	
	@Query( value = "select * from subject s " +
            "join teacher_subject ts " +
            "   on s.id = ts.subject_id " +
            "where ts.teacher_id = ?1", nativeQuery = true)
	List<Subject> getTeacherSubjects(int teacherId);
	
	@Query( value = "select * from subject s " +
            "join teacher_subject ts " +
            "   on s.id = ts.subject_id " +
            "where ts.teacher_id = ?1 and s.title like ?2%", nativeQuery = true)
	List<Subject> filterTeacherSubjects(int teacherId, String searchText);
	
	@Query( value = "select subject.title from subject " +
            "where subject.title not in " +
			"(select subject.title from subject join teacher_subject " +
            "on subject.id = teacher_subject.subject_id " +
            "where teacher_subject.teacher_id = ?1) and " +
            "subject.title like ?2%", nativeQuery = true)
	List<String> filterNotTeacherSubjects(int teacherId, String searchText);
	
	@Query( value = "select * from subject s " +
            "join student_subject ss " +
            "   on s.id = ss.subject_id " +
            "where ss.student_id = ?1 and s.title like ?2%", nativeQuery = true)
	List<Subject> filterStudentSubjects(int studentId, String searchText);
	
	@Query( value = "select subject.title from subject " +
            "where subject.title not in " +
			"(select subject.title from subject join student_subject " +
            "on subject.id = student_subject.subject_id " +
            "where student_subject.student_id = ?1) and " +
            "subject.title like ?2%", nativeQuery = true)
	List<String> filterNotStudentSubjects(int studentId, String searchText);
	
	

}


























