package com.mvujanic.student.service.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mvujanic.student.service.entities.User;

@Repository
public interface LoginRepository extends JpaRepository<User, Integer> {
	User findByUsernameOrEmail(String username, String email);
}
