package com.mvujanic.student.service.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mvujanic.student.service.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

	User findByUsernameOrEmail(String username, String email);

	User findByUsername(String username);

	User findByEmail(String email);

	@Query("SELECT u FROM User u WHERE u.userCategory.id = 1")
	List<User> findAllAdmins();

	@Query("SELECT u FROM User u WHERE u.userCategory.id = 2")
	List<User> findAllTeachers();

	@Query("SELECT u FROM User u WHERE u.userCategory.id = 3")
	List<User> findAllStudents();

	@Query("SELECT u FROM User u WHERE u.userCategory.id = 2 OR u.userCategory.id = 3")
	List<User> findAllTeachersAndStudents();

	@Query("SELECT u FROM User u WHERE u.userCategory.id = ?1 AND u.username LIKE ?2%")
	List<User> filterUsersByUsername(int userCategoryId, String searchText);

	@Query("SELECT u FROM User u WHERE u.userCategory.id = ?1 AND u.firstName LIKE ?2%")
	List<User> filterUsersByFirstName(int userCategoryId, String searchText);

	@Query("SELECT u FROM User u WHERE u.userCategory.id = ?1 AND u.lastName LIKE ?2%")
	List<User> filterUsersByLastName(int userCategoryId, String searchText);

	@Query("SELECT u FROM User u WHERE u.userCategory.id = ?1 AND u.email LIKE ?2%")
	List<User> filterUsersByEmail(int userCategoryId, String searchText);

	@Query("SELECT u FROM User u WHERE (u.userCategory.id = 2 OR u.userCategory.id = 3) AND u.username LIKE ?1%")
	List<User> filterUsersByUsername(String searchText);

	@Query("SELECT u FROM User u WHERE (u.userCategory.id = 2 OR u.userCategory.id = 3) AND u.firstName LIKE ?1%")
	List<User> filterUsersByFirstName(String searchText);

	@Query("SELECT u FROM User u WHERE (u.userCategory.id = 2 OR u.userCategory.id = 3) AND u.lastName LIKE ?1%")
	List<User> filterUsersByLastName(String searchText);

	@Query("SELECT u FROM User u WHERE (u.userCategory.id = 2 OR u.userCategory.id = 3) AND u.email LIKE ?1%")
	List<User> filterUsersByEmail(String searchText);
	
	@Modifying
	@Query("UPDATE User SET firstName = ?1, lastName = ?2, password = ?3 WHERE username = ?4")
	void updateUser(String firstName, String lastName, String password, String username);
}
