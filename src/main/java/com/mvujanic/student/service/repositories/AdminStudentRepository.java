package com.mvujanic.student.service.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mvujanic.student.service.entities.Student;

@Repository
public interface AdminStudentRepository extends JpaRepository<Student, Integer> {

	public Student findStudentByUsername(String studentUsername);
	
	public Student findById(String studentId);
}
