package com.mvujanic.student.service.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "subject")
public class Subject implements Comparable<Subject>{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "title")
	private String title;

	@JsonIgnore
	@ManyToMany(fetch=FetchType.LAZY,
			cascade = {CascadeType.PERSIST, CascadeType.MERGE,
					CascadeType.DETACH, CascadeType.REFRESH	})
	@JoinTable(name = "teacher_subject", joinColumns = @JoinColumn(name = "subject_id"), inverseJoinColumns = @JoinColumn(name = "teacher_id"))
	private List<Teacher> teachers;
	
	@JsonIgnore
	@ManyToMany(fetch=FetchType.LAZY,
			cascade = {CascadeType.PERSIST, CascadeType.MERGE,
					CascadeType.DETACH, CascadeType.REFRESH	})
	@JoinTable(name="student_subject", joinColumns = @JoinColumn(name = "subject_id"), inverseJoinColumns = @JoinColumn(name="student_id"))
	private List<Student> students;
	
	public Subject() {

	}
	
	public void addTeacher(Teacher teacher) {
		if (teachers == null) {
			teachers = new ArrayList<Teacher>();			
		}
		
		teachers.add(teacher);
	}
	
	public void removeTeacher(Teacher teacher) {
				
		teachers.remove(teacher);
	}
	
	public void addStudent(Student student) {
		if (students == null) {
			students = new ArrayList<Student>();			
		}
		
		students.add(student);
	}
	
	public void removeStudent(Student student) {
		
		students.remove(student);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Teacher> getTeachers() {
		Collections.sort(teachers);
		
		return teachers;
	}

	public void setTeachers(List<Teacher> teachers) {
		this.teachers = teachers;
	}

	@Override
	public int compareTo(Subject subject) {
		return this.getTitle().compareToIgnoreCase(subject.getTitle());
	}

}
